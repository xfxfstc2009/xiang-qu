//
//  PhotoLib.h
//  PhotoHandler
//
//  Created by 朱雀 on 14-12-5.
//  Copyright (c) 2014年 Ouer. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^resultPhotoBlock) (UIImage *image);

@interface PhotoLib : UIViewController

@property (nonatomic, weak) UIViewController *parentVC;
@property (nonatomic, copy) resultPhotoBlock resultPhotoBlock;

@end
