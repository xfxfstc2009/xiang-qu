//
//  StatConfig.h
//  TA-SDK
//
//  Created by WQY on 12-11-5.
//  Copyright (c) 2012年 WQY. All rights reserved.
//

#import <Foundation/Foundation.h>

// - 腾讯云统计
#define mta_AppKey          @"I9KM2B3D7TDR"             //  云统计AppKey
#define mta_QQ              @"1035500939"               //  QQ号码

// - 腾讯云统计接口
//==================================App事件云统计接口=====================================
#define sWeibo              @"s-001"                    //  分享到微博
#define sWeixin             @"s-002"                    //  分享到微信好友
#define sCircle             @"s-003"                    //  分享到微信朋友圈

//=================================页面访问统计接口=======================================
#define pThemeDetail        @"p1-001"                   //  查看专题详情
#define pGuang              @"p1-0021"                  //  逛商品
#define pFaverFriends       @"p1-0022"                  //  朋友喜欢
#define pFaverAll           @"p1-0023"                  //  大家喜欢
#define pTopicHot           @"p1-0031"                  //  查看热门话题
#define pTopicMy            @"p1-0032"                  //  查看我的话题
#define pTopicDetail        @"p1-0033"                  //  查看话题详情
#define pMySetting          @"p1-0041"                  //  我的-设置
#define pMyMessage          @"p1-0042"                  //  我的-消息
#define pMyEditing          @"p1-0043"                  //  我的-编辑个人资料
#define pMyFansList         @"p1-0044"                  //  我的-查看粉丝列表
#define pMyFollowList       @"p1-0045"                  //  我的-查看关注列表

typedef enum {
    MTA_STRATEGY_INSTANT = 1,            //实时上报
    MTA_STRATEGY_BATCH = 2,              //批量上报，达到缓存临界值时触发发送
    MTA_STRATEGY_APP_LAUNCH = 3,         //应用启动时发送
    MTA_STRATEGY_ONLY_WIFI = 4,          //仅在WIFI网络下发送
    MTA_STRATEGY_PERIOD = 5,             //每间隔一定最小时间发送，默认24小时
    MTA_STRATEGY_DEVELOPER = 6,          //开发者在代码中主动调用发送行为
    MTA_STRATEGY_ONLY_WIFI_NO_CACHE = 7  //仅在WIFI网络下发送, 发送失败以及非WIFI网络情况下不缓存数据
} MTAStatReportStrategy;

@interface MTAConfig : NSObject
@property BOOL debugEnable;                     //debug开关
@property uint32_t sessionTimeoutSecs;          //Session超时时长，默认30秒
@property MTAStatReportStrategy reportStrategy;    //统计上报策略
@property (nonatomic, retain) NSString* appkey; //应用的统计AppKey
@property (nonatomic, retain) NSString* channel;//渠道名，默认为"appstore"
@property uint32_t maxStoreEventCount;          //最大缓存的未发送的统计消息，默认1024
@property uint32_t maxLoadEventCount;           //一次最大加载未发送的缓存消息，默认30
@property uint32_t minBatchReportCount;         //统计上报策略为BATCH时，触发上报时最小缓存消息数，默认30
@property uint32_t maxSendRetryCount;           //发送失败最大重试数，默认3
@property uint32_t sendPeriodMinutes;           //上报策略为PERIOD时发送间隔，单位分钟，默认一天（1440分钟）
@property uint32_t maxParallelTimingEvents;     //最大并行统计的时长事件数，默认1024
@property BOOL  smartReporting;                 //智能上报开关：在WIFI模式下实时上报，默认TRUE
@property BOOL  autoExceptionCaught;            //智能捕获未catch的异常，默认TRUE；设置为False需要在startWithAppkey前调用
@property uint32_t maxReportEventLength;        //最大上报的单条event长度，超过不上报
@property (nonatomic, retain) NSString* qq;           //QQ号或者帐号
@property (nonatomic, retain) NSString* account;      //帐号
@property int8_t accountType;                       //帐号类型
@property (nonatomic, retain) NSString* accountExt;   //帐号的扩展信息
@property BOOL statEnable;
//@property BOOL enableReportIDFA;               //是否上报IDFA，默认为NO，注意：苹果限制只有带广告的APP才能获取，开启前请确保符合要求67

@property (nonatomic, retain) NSString* customerUserID;
@property (nonatomic, retain) NSString* customerAppVersion;
@property (nonatomic, retain) NSString* ifa;
@property (nonatomic, retain) NSString* pushDeviceToken;

@property (nonatomic, retain) NSString* statReportURL; //自定义的上报url
@property int32_t maxSessionStatReportCount;

@property (nonatomic,retain) NSString* op;          //运营商
@property (nonatomic,retain) NSString* cn;          //网络类型
@property (nonatomic,retain) NSString* sp;   //测速结果

-(id) init;
-(NSString*)getCustomProperty:(NSString*) key default:(NSString*) v;
+(id) getInstance;
@end
