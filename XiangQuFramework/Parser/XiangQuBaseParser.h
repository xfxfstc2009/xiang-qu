//
//  XiangQuBaseParser.h
//  XiangQu
//
//  Created by yandi on 14/10/25.
//  Copyright (c) 2014年 Qiuyin. All rights reserved.
//

#import "XiangQuParse.h"
#import <Foundation/Foundation.h>

@interface XiangQuBaseParser : NSObject<XiangQuParse>
@property (nonatomic, copy) NSString *msg;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, assign) BOOL isSucceed;
@property (nonatomic, assign) BOOL isLoadingMore;
@property (nonatomic, assign) NSUInteger orderType;

- (NSDictionary *)mapperKey;
- (void)parserBody:(NSDictionary *)parserDict;
@end
