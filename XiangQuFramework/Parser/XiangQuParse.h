//
//  XiangQuParse.h
//  XiangQu
//
//  Created by yandi on 14/10/25.
//  Copyright (c) 2014年 Qiuyin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XiangQuParse <NSObject>
- (void)parserBody:(NSDictionary *)parserDict;
@end
