//
//  XiangQuParserFactory.m
//  XiangQu
//
//  Created by yandi on 14/10/25.
//  Copyright (c) 2014年 Qiuyin. All rights reserved.
//

// parser Header Sets
#import "XiangQuSkuParser.h"
#import "XiangQuZoneParser.h"
#import "XiangQuMenuParser.h"
#import "XiangQuFaverParser.h"
#import "XiangQuLoginParser.h"
#import "XiangQuThemeParser.h"
#import "XiangQuOrderParser.h"
#import "XiangQuTopicParser.h"
#import "XiangQuShareParser.h"
#import "XiangQuSocialParser.h"
#import "XiangQuFollowParser.h"
#import "XiangQuAvatarParser.h"
#import "XiangQuVersionParser.h"
#import "XiangQuProfileParser.h"
#import "XiangQuDoFaverParser.h"
#import "XiangQuCommentParser.h"
#import "XiangQuProductParser.h"
#import "XiangQuAddressParser.h"
#import "XiangQuProductParser.h"
#import "XiangQuNewPostParser.h"
#import "XiangQuShopCartParser.h"
#import "XiangQuNewTopicParser.h"
#import "XiangQuPostFaverParser.h"
#import "XiangQuRecommendParser.h"
#import "XiangQuFriendFeedParser.h"
#import "XiangQuOrderDetailParser.h"
#import "XiangQuThemeDetailParser.h"
#import "XiangQuPostCommentParser.h"
#import "XiangQuOrderCommentParser.h"
#import "XiangQuProductDetailParser.h"
#import "XiangQuProductDetailParser.h"
#import "XiangQuDynamicNoticeParser.h"
#import "XiangQuPrivateMessageParser.h"
#import "XiangQuDynamicFriendsParser.h"
#import "XiangQuOrderDetailCommentParser.h"

#import "XiangQuParserFactory.h"

static XiangQuParserFactory *xiangQuParserFactory = nil;
@implementation XiangQuParserFactory

#pragma mark -sharedInstance
+ (XiangQuParserFactory *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        xiangQuParserFactory = [[XiangQuParserFactory alloc] init];
    });
    return xiangQuParserFactory;
}

#pragma mark -parserWithURL
- (id<XiangQuParse>)parserWithURL:(NSString *)url {
    if ([url isEqualToString:theme_URL]) {
        XiangQuThemeParser *themeParser = [[XiangQuThemeParser alloc] init];
        return (id<XiangQuParse>)themeParser;
    } else if ([url isEqualToString:magezine_URL]) {
        XiangQuThemeDetailParser *themeDetailParser = [[XiangQuThemeDetailParser alloc] init];
        return (id<XiangQuParse>)themeDetailParser;
    } else if ([url isEqualToString:login_URL] || [url isEqualToString:regSetUsr_URL]) {
        XiangQuLoginParser *loginParser = [[XiangQuLoginParser alloc] init];
        return (id<XiangQuParse>)loginParser;
    } else if ([url isEqualToString:usr_URL]) {
        XiangQuProfileParser *profileParser = [[XiangQuProfileParser alloc] init];
        return (id<XiangQuParse>)profileParser;
    } else if ([url isEqualToString:usrFaver_URL]) {
        XiangQuFaverParser *faverParser = [[XiangQuFaverParser alloc] init];
        return (id<XiangQuParse>)faverParser;
    } else if ([url isEqualToString:order_URL]) {
        XiangQuOrderParser *orderParser = [[XiangQuOrderParser alloc] init];
        return (id<XiangQuParse>)orderParser;
    } else if ([url isEqualToString:faver_URL] || [url isEqualToString:defaver_URL] || [url isEqualToString:postFaverAdd_URL] || [url isEqualToString:postFaverDelete_URL]) {
        XiangQuDoFaverParser *doFaverParser = [[XiangQuDoFaverParser alloc] init];
        return (id<XiangQuParse>)doFaverParser;
    } else if ([url isEqualToString:getShareText_URL]) {
        XiangQuSocialParser *socialParser = [[XiangQuSocialParser alloc] init];
        return (id<XiangQuParse>)socialParser;
    } else if ([url isEqualToString:mytopic_URL] || [url isEqualToString:hottopic_URL]) {
        XiangQuTopicArrayParser *topicArrayDict = [[XiangQuTopicArrayParser alloc]init];
        return (id<XiangQuParse>)topicArrayDict;
    } else if ([url isEqualToString:share_URL]) {
        XiangQuShareParser *shareParser = [[XiangQuShareParser alloc] init];
        return (id<XiangQuParse>)shareParser;
    } else if ([url isEqualToString:topicDetail_URL]) {
        XiangQuTopicDetailParser *topicModel = [[XiangQuTopicDetailParser alloc]init];
        return (id<XiangQuParse>)topicModel;
    } else if ([url isEqualToString:productComment_URL]) {
        XiangQuOrderDetailCommentParser *orderDetailCommentParser = [[XiangQuOrderDetailCommentParser alloc] init];
        return (id<XiangQuParse>)orderDetailCommentParser;
    } else if ([url isEqualToString:usrInfo_URL]) {
        XiangQuFollowParser *followParser = [[XiangQuFollowParser alloc] init];
        return (id<XiangQuParse>)followParser;
    } else if ([url isEqualToString:orderdetail_URL]) {
        XiangQuOrderDetailParser *orderDetailParser = [[XiangQuOrderDetailParser alloc] init];
        return (id<XiangQuParse>)orderDetailParser;
    } else if ([url isEqualToString:comment_URL]) {
        XiangQuCommentParser *commentParser = [[XiangQuCommentParser alloc] init];
        return (id<XiangQuParse>)commentParser;
    } else if ([url isEqualToString:postCommentList_URL]) {
        XiangQuPostCommentListParser *commentParser = [[XiangQuPostCommentListParser alloc]init];
        return (id<XiangQuParse>)commentParser;
    } else if ([url isEqualToString:postFaverList_URL]) {
        XiangQuPostFaverListParser *faverParser = [[XiangQuPostFaverListParser alloc]init];
        return (id<XiangQuParse>)faverParser;
    } else if ([url isEqualToString:product_URL] || [url isEqualToString:activity_URL]) {
        XiangQuProductParser *productParser = [[XiangQuProductParser alloc] init];
        return (id<XiangQuParse>)productParser;
    } else if ([url isEqualToString:everyonelike_URL]) {
        XiangQuFriendFeedParser *friendFeedParser = [[XiangQuFriendFeedParser alloc] init];
        return (id<XiangQuParse>)friendFeedParser;
    } else if ([url isEqualToString:friendfeed_URL]) {
        XiangQuDynamicFriendsParser *dynamicFriendsParser = [[XiangQuDynamicFriendsParser alloc] init];
        return (id<XiangQuParse>)dynamicFriendsParser;
    } else if ([url isEqualToString:getcate_URL]) {
        XiangQuMenuParser *menuParser = [[XiangQuMenuParser alloc] init];
        return (id<XiangQuParse>)menuParser;
    } else if ([url isEqualToString:privateMsgList_URL]) {
        XiangQuPrivateMessageParser *privateMsgParser = [[XiangQuPrivateMessageParser alloc]init];
        return (id<XiangQuParse>)privateMsgParser;
    } else if ([url isEqualToString:privateChatList_URL]) {
        XiangQuPrivateChatParser *chatParser = [[XiangQuPrivateChatParser alloc]init];
        return (id<XiangQuParse>)chatParser;
    } else if ([url isEqualToString:orderMsgList_URL]) {
        XiangQuOrderCommentListParser *orderListParser = [[XiangQuOrderCommentListParser alloc]init];
        return (id<XiangQuParse>)orderListParser;
    } else if ([url isEqualToString:dynamic_notice_URL]) {
        XiangQuDynamicNoticeParser *dynamicNoticeParser = [[XiangQuDynamicNoticeParser alloc]init];
        return (id<XiangQuParse>)dynamicNoticeParser;
    } else if ([url isEqualToString:productRommend_URL]) {
        XiangQuRecommendParser *recommendParser = [[XiangQuRecommendParser alloc] init];
        return (id<XiangQuParse>)recommendParser;
    } else if([url isEqualToString:productDetail_URL]) {
        XiangQuProductDetailParser *productDetailParser = [[XiangQuProductDetailParser alloc]init];
        return (id<XiangQuParse>)productDetailParser;
    } else if ([url isEqualToString:address_URL]) {
        XiangQuAddressParser *addressParser = [[XiangQuAddressParser alloc] init];
        return (id<XiangQuParse>)addressParser;
    } else if ([url rangeOfString:@"zone"].location != NSNotFound && [url rangeOfString:@"children"].location != NSNotFound) {
        XiangQuZoneParser *zoneParser = [[XiangQuZoneParser alloc] init];
        return (id<XiangQuParse>)zoneParser;
    } else if([url hasSuffix:@"skus"]) {
        XiangQuSkuParser *skuParser = [[XiangQuSkuParser alloc] init];
        return (id<XiangQuParse>)skuParser;
    } else if ([url isEqualToString:postAddNew_URL]) {
        XiangQuNewPostParser *postParser = [[XiangQuNewPostParser alloc]init];
        return (id<XiangQuParse>)postParser;
    } else if ([url  isEqualToString:topicCreate_URL]) {
        XiangQuNewTopicParser *topicParser = [[XiangQuNewTopicParser alloc]init];
        return (id<XiangQuParse>)topicParser;
    } else if ([url isEqualToString:usrsetting_URL]) {
        XiangQuAvatarParser *avatarParser = [[XiangQuAvatarParser alloc] init];
        return (id<XiangQuParse>)avatarParser;
    } else if ([url isEqualToString:version_URL]) {
        XiangQuVersionParser *versionParser = [[XiangQuVersionParser alloc] init];
        return (id<XiangQuParse>)versionParser;
    } else if ([url isEqualToString:productCommentList_URL]){
        XiangquProductCommentParser *commentParser = [[XiangquProductCommentParser alloc] init];
        return (id<XiangQuParse>)commentParser;
    } else if ([url isEqualToString:shopCartList_URL]){
        XiangQuShopCartParser *shopCartParper = [[XiangQuShopCartParser alloc] init];
        return (id<XiangQuParse>)shopCartParper;
    } else if ([url isEqualToString:postDetail_URL]) {
        XiangQuPostDetailParser *postDetailParser = [[XiangQuPostDetailParser alloc]init];
        return (id<XiangQuParse>)postDetailParser;
    }
    
    XiangQuBaseParser *baseParser = [[XiangQuBaseParser alloc] init];
    return baseParser;
}
@end
