//
//  XiangQuParserFactory.h
//  XiangQu
//
//  Created by yandi on 14/10/25.
//  Copyright (c) 2014年 Qiuyin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface XiangQuParserFactory : NSObject

+ (XiangQuParserFactory *)sharedInstance; //singleton
- (id<XiangQuParse>)parserWithURL:(NSString *)url;
@end
