//
//  XiangQuHttpClient.h
//  XiangQu
//
//  Created by yandi on 14/10/24.
//  Copyright (c) 2014年 Qiuyin. All rights reserved.
//

@interface XiangQuHttpClient : AFHTTPRequestOperationManager

+ (NSString *)actionCustomUsrAgent;

- (void)uploadDataWithURL:(NSString *)url
                    paras:(NSDictionary *)parasDict
            formatterData:(NSArray *)data
                  success:(void(^)(AFHTTPRequestOperation *operation,NSObject *parserObject))success
                  failure:(void(^)(AFHTTPRequestOperation *operation,NSError *requestErr))failure;

- (AFHTTPRequestOperation *)requestWithURL:(NSString *)url
                                     paras:(NSMutableDictionary *)parasDict
                                   success:(void(^)(AFHTTPRequestOperation *operation,NSObject *parserObject))success
                                   failure:(void(^)(AFHTTPRequestOperation *operation,NSError *requestErr))failure;


@end
